// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Edit this
	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootScene")));

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(RootComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(RootComponent);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnExitTriggerBeginOverlap);

	ObjectSpawn = CreateDefaultSubobject<UBoxComponent>(TEXT("ObstacleSpawn"));
	ObjectSpawn->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	SpawnActor();
}

FTransform ATile::GetAttachPointTransform()
{
	return AttachPoint->GetRelativeTransform();
}

void ATile::OnExitTriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* player = Cast<ARunCharacter>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Player has passed an end trigger!"));
		ExitTriggeredEvent.Broadcast(this);
	}
}

void ATile::SpawnActor()
{
	if (rand() % 100 < PickupSpawnChance)
		SpawnPickup();
	else
		SpawnObstacle();
}

void ATile::SpawnObstacle()
{
	FVector SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(ObjectSpawn->GetComponentLocation(), ObjectSpawn->GetScaledBoxExtent());
	int i = UKismetMathLibrary::RandomIntegerInRange(0, ObstacleRefArray.Num() - 1);

	SpawnedObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleRefArray[i], FTransform(SpawnLocation));
	SpawnedObstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void ATile::SpawnPickup()
{
	FVector SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(ObjectSpawn->GetComponentLocation(), ObjectSpawn->GetScaledBoxExtent());
	int i = UKismetMathLibrary::RandomIntegerInRange(0, PickupRefArray.Num() - 1);

	SpawnedPickup = GetWorld()->SpawnActor<APickup>(PickupRefArray[i], FTransform(SpawnLocation));
	SpawnedPickup->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void ATile::SetDeathTimer()
{
	GetWorld()->GetTimerManager().SetTimer(TriggerDestroyTimerHandle, this, &ATile::DestroySelf, 2.0f, false);
}

void ATile::DestroySelf()
{
	if(SpawnedObstacle != NULL)
		SpawnedObstacle->Destroy();

	if(SpawnedPickup != NULL)
		SpawnedPickup->Destroy();

	Destroy();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

