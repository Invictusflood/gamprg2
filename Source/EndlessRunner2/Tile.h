// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnExitTriggered, ATile*, TileToDestroy); //Pass Parameter here to destroy via gamemode

UCLASS()
class ENDLESSRUNNER2_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	FTransform GetAttachPointTransform();

	UPROPERTY(BlueprintAssignable, Category = "Event Dispatcher")
		FOnExitTriggered ExitTriggeredEvent;

	UFUNCTION()
		void SetDeathTimer();

	UFUNCTION()
		void DestroySelf();

protected:
	FTimerHandle TriggerDestroyTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int PickupSpawnChance;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Arrow Component")
		class UArrowComponent* AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Exit Trigger")
		class UBoxComponent* ExitTrigger;

	UPROPERTY(EditAnywhere, Category = "Obstacle Spawn")
		class UBoxComponent* ObjectSpawn;

	UPROPERTY(EditAnywhere, Category = "Obstacle")
		class AObstacle* SpawnedObstacle;

	UPROPERTY(EditAnywhere, Category = "Obstacle")
		class APickup* SpawnedPickup;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacles Refs")
		TArray<TSubclassOf<class AObstacle>> ObstacleRefArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup Refs")
		TArray<TSubclassOf<class APickup>> PickupRefArray;

	UFUNCTION()
		void OnExitTriggerBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void SpawnActor();

	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
		void SpawnPickup();

};
