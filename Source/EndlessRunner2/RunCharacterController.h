// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"
//#include "RunCharacter.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER2_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacterController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	class ARunCharacter* ControlledPawn;

	//Input bindings
	void MoveForward(float scale);
	void MoveRight(float scale);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

};