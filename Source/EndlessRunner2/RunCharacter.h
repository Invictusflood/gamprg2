// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS()
class ENDLESSRUNNER2_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

	bool IsAlive;

	UPROPERTY(BlueprintReadWrite)
		int Coins;

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
		FOnDeath DeathEvent;

	UFUNCTION(BlueprintCallable)
		virtual void AddCoin();

	UFUNCTION(BlueprintCallable)
		virtual void Die();

protected:

	UPROPERTY(EditAnywhere, Category = "Camera")
		class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, Category = "Spring Arm")
		class USpringArmComponent* SpringArm;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
