// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlessRunner2GameMode.generated.h"

UCLASS(minimalapi)
class AEndlessRunner2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEndlessRunner2GameMode();
};



