// Copyright Epic Games, Inc. All Rights Reserved.

#include "EndlessRunner2GameMode.h"
#include "EndlessRunner2Character.h"
#include "UObject/ConstructorHelpers.h"

AEndlessRunner2GameMode::AEndlessRunner2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
