// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"

ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
	//PrimaryActorTick.bStartWithTickEnabled = true;
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	//Get RunCharacter here
	ControlledPawn = Cast<ARunCharacter>(GetPawn());
}

void ARunCharacterController::MoveForward(float scale)
{
	ControlledPawn->AddMovementInput(ControlledPawn->GetActorForwardVector(), scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	ControlledPawn->AddMovementInput(ControlledPawn->GetActorRightVector(), scale);
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(ControlledPawn->IsAlive)
		MoveForward(1.0);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}


