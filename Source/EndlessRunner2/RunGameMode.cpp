// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"

// Called when the game starts or when spawned
void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();
	ATileClassRef = ATile::StaticClass();


	for (int i = 0; i < 4; i++)
	{
		SpawnTile(NULL);
	}
}

void ARunGameMode::SpawnTile(ATile* TileToDestroy)
{
	AActor* SpawnedActor = SpawnedActor = GetWorld()->SpawnActor<AActor>(ActorToSpawn, TileSpawnTransform);
	ATile* SpawnedTile = Cast<ATile>(SpawnedActor);

	if (SpawnedTile != NULL)
	{
		TileSpawnTransform.Accumulate(SpawnedTile->GetAttachPointTransform());
		SpawnedTile->ExitTriggeredEvent.AddDynamic(this, &ARunGameMode::SpawnTile);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Event should have binded..."));
	}
	else
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Tile failed to spawn"));

	if (TileToDestroy != NULL)
		TileToDestroy->SetDeathTimer();
}