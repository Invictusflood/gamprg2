// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "Engine/StaticMesh.h"
#include "RunCharacter.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->OnComponentHit.AddDynamic(this, &AObstacle::OnTrigger);
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

//void AObstacle::OnHit(ARunCharacter* player)
//{
//}

void AObstacle::OnTrigger(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (ARunCharacter* player = Cast<ARunCharacter>(OtherActor))
		OnHit(player);
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

